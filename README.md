Test Basket
```bash
cp .env.example .env

docker-compose up -d

cp backend/.env.example backend/.env
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate --seed

cp frontend/.env.example frontend/.env
```

Open browser:

`http://localhost:8000`

Run tests: 
```bash
docker-compose exec app vendor/bin/phpunit
```
