import Vue from 'vue';
import Router from 'vue-router';
import Home from '../pages/Home';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: '/',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/baskets',
            name: 'baskets',
            component: () => import('../pages/Basket/Baskets')
        },
        {
            path: '/baskets/:id',
            name: 'show-basket',
            component: () => import('../pages/Basket/Basket')
        },
    ]
});
