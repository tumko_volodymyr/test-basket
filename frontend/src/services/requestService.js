import axios from 'axios';
import config from "@/config";

const axiosInstance = axios.create({baseURL: config.getApiUrl()});
axiosInstance.interceptors
    .response
    .use(
        response => response.data,
        errorResponse => {
            const { response } = errorResponse;

            if (!response) {
                return Promise.reject(new Error('Unexpected error!'));
            }
            return Promise.reject(response.data.error.message);
        },
    );

axiosInstance.interceptors.request.use((config) => {

    config.headers['X-Requested-With'] = 'XMLHttpRequest';
    config.headers['Content-type'] = 'application/json';
    config.headers['Accept'] = 'application/json';

    return config;
});

axiosInstance.interceptors.response.use(response => response.data);

const get = (url, headers = {}, params = {}) => {
    return axiosInstance.get(url, {
        params: params,
        headers: headers
    });
};

const post = (url, data, headers = {}) => {
    return axiosInstance.post(url, data, {
        headers: headers
    });
};

const put = (url, data, headers = {}) => {
    return axiosInstance.put(url, data, {
        headers: headers
    });
};

const destroy = url => axiosInstance.delete(url);

const requestService = {
    post,
    get,
    put,
    delete: destroy
};

export default requestService;
