import Vue from 'vue';
import App from './App.vue';
import vuetify from "./plugins/vuetify";
import router from "./plugins/router";
import store from './store';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import 'vuetify/dist/vuetify.min.css';

Vue.component('FontAwesomeIcon', FontAwesomeIcon);

new Vue({
    render: h => h(App),
    vuetify,
    router,
    store,
}).$mount('#app');
