import Vue from 'vue';
import Vuex from 'vuex';
import baskets from './baskets';
import items from './items';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        baskets,
        items
    },
});
