import requestService from '../../services/requestService';
import {SET_BASKETS, SET_BASKET, ADD_BASKET, DELETE_BASKET} from './mutationTypes';

export default {
    async fetchBaskets({ commit }) {
        try {
            const baskets = await requestService.get('/baskets');
            commit(SET_BASKETS, baskets);
            return Promise.resolve(baskets);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async fetchBasketById({ commit }, id) {
        try {
            const basket = await requestService.get(`/baskets/${id}`);
            commit(ADD_BASKET, basket);
            return Promise.resolve(basket);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async addBasket({ commit }, {  name, capacity }) {
        try {
            const basket = await requestService.post('/baskets', { name , max_capacity: capacity });
            commit(ADD_BASKET, basket);
            return Promise.resolve(basket);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async deleteBasket({ commit }, id) {
        try {
            await requestService.delete(`/baskets/${id}`);

            commit(DELETE_BASKET, id);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async updateBasket({ commit }, { id, name }) {
        try {
            const basket = await requestService.put(`/baskets/${id}`, { name });

            commit(SET_BASKET, basket);
            return Promise.resolve(basket);
        } catch (error) {
            return Promise.reject(error);
        }
    },
};
