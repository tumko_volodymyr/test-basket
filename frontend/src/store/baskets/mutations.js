import {SET_BASKET, SET_BASKETS, ADD_BASKET, DELETE_BASKET} from './mutationTypes';

export default {
    [SET_BASKETS]: (state, baskets) => {
        baskets.forEach(basket => {
            state.baskets = {
                ...state.baskets,
                [basket.id]: basket
            };
        });
    },

    [ADD_BASKET]: (state, basket) => {
        state.baskets = {
            ...state.baskets,
            [basket.id]: basket
        };
    },

    [DELETE_BASKET]: (state, id) => {
        const baskets = { ...state.baskets };
        delete baskets[id];
        state.baskets = baskets ;
    },


    [SET_BASKET]: (state, basket) => {
        state.baskets = {
            ...state.baskets,
            [basket.id]: basket
        };
    },
};
