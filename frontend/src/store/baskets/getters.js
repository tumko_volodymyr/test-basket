
export default {
    getBaskets: state => state.baskets,
    getBasket: state => id => state.baskets[id],
};
