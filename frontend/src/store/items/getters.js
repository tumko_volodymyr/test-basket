export default {
    getItemsByBasketId: state => id => Object
        .values(state.items)
        .filter( item => item.basket_id === parseInt(id))
        .sort(
            (a, b) =>  a - b
        ),
};
