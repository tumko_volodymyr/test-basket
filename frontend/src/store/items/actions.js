import requestService from '../../services/requestService';
import {SET_ITEMS, DELETE_ITEM} from './mutationTypes';

export default {
    async fetchItemsByBasketId({ commit }, id) {
        try {
            const items = await requestService.get(`/baskets/${id}/items`);
            commit(SET_ITEMS, items);
            return Promise.resolve(items);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async addItems({ commit }, items) {
        try {
            const newItems = await requestService.post('/items/array', {items: items});
            commit(SET_ITEMS, newItems);
            return Promise.resolve(newItems);
        } catch (error) {
            return Promise.reject(error);
        }
    },

    async deleteItem({ commit }, id) {
        try {
            await requestService.delete(`/items/${id}`);

            commit(DELETE_ITEM, id);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    },
};
