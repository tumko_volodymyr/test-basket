import {SET_ITEMS, DELETE_ITEM} from './mutationTypes';

export default {
    [SET_ITEMS]: (state, items) => {
        items.forEach(item => {
            state.items = {
                ...state.items,
                [item.id]: item
            };
        });
    },

    [DELETE_ITEM]: (state, id) => {
        const items = { ...state.items };
        delete items[id];
        state.items = items ;
    },
};
