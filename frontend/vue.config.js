
module.exports = {
    devServer: {
        host: '0.0.0.0',
        disableHostCheck: true,
        overlay: {
            warnings: true,
            errors: true
        }
    },
    configureWebpack: (config) => {
        if (process.env.NODE_ENV !== "production") {
            config.devtool = 'source-map';
        }
    }
};