<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::group([
        'namespace' => 'Api',
    ], function () {
        Route::group([
            'prefix' => 'baskets'
        ], function () {
            Route::post('/', 'BasketController@create');
            Route::get('/', 'BasketController@all');
            Route::get('/{id}', 'BasketController@show');
            Route::delete('/{id}', 'BasketController@delete');
            Route::put('/{id}', 'BasketController@update');
            Route::get('/{id}/items', 'BasketController@basketItems');

        });
        Route::group([
            'prefix' => 'items'
        ], function () {
            Route::delete('/{id}', 'ItemController@delete');
            Route::post('/array', 'ItemController@createItems');

        });
    });
});
