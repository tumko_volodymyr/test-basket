<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Entities\Basket;
use Faker\Generator as Faker;

$factory->define(Basket::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord,
        'max_capacity' => $faker->numberBetween(100, 10000),
    ];
});
