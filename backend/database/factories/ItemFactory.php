<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Contracts\Entities\ItemInterface;
use App\Model\Entities\Basket;
use App\Model\Entities\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'type' => ItemInterface::TYPE_APPLE,
        'weight' => $faker->numberBetween(50, 150),
        'basket_id' => function () {
            return Basket::query()->inRandomOrder()->first()->id;
        },
    ];
});
