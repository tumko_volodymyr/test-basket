<?php

use App\Model\Entities\Basket;
use App\Model\Entities\ItemApple;
use Illuminate\Database\Seeder;

class BasketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Basket::class, 5)
            ->create()
            ->each(function (Basket $basket) {
                $basket->items()->createMany(
                    factory(ItemApple::class, 3)->make()->toArray()
                );
            });
    }
}
