<?php

namespace Tests\Unit;

use App\Model\Contracts\Repositories\BasketRepositoryInterface;
use App\Model\Entities\Basket;
use App\Model\Exceptions\BasketNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EloquentBasketRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /** @var BasketRepositoryInterface */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(BasketRepositoryInterface::class);
    }

    public function testGetByIdExist()
    {
        $data = $this->data();
        factory(Basket::class)->create($data);
        $basket = $this->repository->getById(1);
        $this->assertInstanceOf(Basket::class, $basket);
    }

    public function testGetByIdNotExist()
    {
        $this->expectException(BasketNotFoundException::class);
        $this->repository->getById(1);
    }

    public function testSave()
    {
        $data = $this->data();
        $basket = factory(Basket::class)->make($data);
        $dbBasket = $this->repository->save($basket);
        $this->assertInstanceOf(Basket::class, $dbBasket);
        $this->assertDatabaseHas('baskets', $data);
    }

    public function testDelete()
    {
        $data = $this->data();
        $basket = factory(Basket::class)->create($data);
        $this->assertDatabaseHas('baskets', $data);
        $deleted = $this->repository->delete($basket);
        $this->assertTrue($deleted);
        $this->assertDatabaseMissing('baskets', $data);
    }

    public function testDeleteNotExistBasket()
    {
        $data = $this->data();
        $basket = factory(Basket::class)->make($data);
        $deleted = $this->repository->delete($basket);
        $this->assertFalse($deleted);
        $this->assertDatabaseMissing('baskets', $data);
    }

    public function testFindAll()
    {
        factory(Basket::class,7)->create();
        $baskets = $this->repository->findAll();

        $this->assertInstanceOf(Collection::class, $baskets);
        $this->assertNotEmpty($baskets);
        foreach ($baskets as $basket){
            $this->assertInstanceOf(Basket::class, $basket);
        }
    }

    public function testFindAllForEmptyTable()
    {
        $baskets = $this->repository->findAll();

        $this->assertInstanceOf(Collection::class, $baskets);
        $this->assertEmpty($baskets);
    }

    private  function data()
    {
        return [
            'name' => 'first basket',
            'max_capacity' => 1000
        ];
    }
}
