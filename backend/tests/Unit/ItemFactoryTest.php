<?php

namespace Tests\Unit;

use App\Exceptions\AppInvalidArgumentException;
use App\Model\Contracts\Entities\ItemInterface;
use App\Model\Contracts\Factories\ItemFactoryInterface;
use App\Model\Entities\Basket;
use App\Model\Exceptions\BasketNotFoundException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemFactoryTest extends TestCase
{
    use RefreshDatabase;

    /** @var ItemFactoryInterface */
    private $factory;
    /** @var Basket */
    private $basket;

    protected function setUp(): void
    {
        parent::setUp();
        $this->basket = factory(Basket::class)->create();
        $this->factory = $this->app->make(ItemFactoryInterface::class);
    }

    public function testItemCreation()
    {
        $weight = 100;
        foreach (ItemInterface::TYPES as $type){
            $item = $this->factory->create($type, $weight, $this->basket->getId());
            $this->assertInstanceOf(ItemInterface::class, $item);
            $this->assertEquals($type, $item->getType());
            $this->assertEquals($weight, $item->getWeight());
            $this->assertEquals($this->basket->getId(), $item->getBasket()->getId());
        }
    }

    public function testItemCreationWithNotExistType()
    {
        $weight = 100;
        $type = 'notExitItemType';
        $this->expectException(AppInvalidArgumentException::class);
        $this->factory->create($type, $weight, $this->basket->getId());
    }

    public function testItemCreationWithInvalidWieght()
    {
        $weight = -100;
        $this->expectException(AppInvalidArgumentException::class);
        foreach (ItemInterface::TYPES as $type){
            $this->factory->create($type, $weight, $this->basket->getId());
        }
    }

    public function testItemCreationWithInvalidBasketId()
    {
        $weight = 100;
        $this->expectException(AppInvalidArgumentException::class);
        foreach (ItemInterface::TYPES as $type){
            $this->factory->create($type, $weight, $this->basket->getId()+1);
        }
    }
}
