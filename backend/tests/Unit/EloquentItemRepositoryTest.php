<?php

namespace Tests\Unit;

use App\Model\Contracts\Repositories\ItemRepositoryInterface;
use App\Model\Entities\Basket;
use App\Model\Entities\Item;
use App\Model\Entities\ItemApple;
use App\Model\Exceptions\ItemNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EloquentItemRepositoryTest extends TestCase
{
    use RefreshDatabase;
    
    /** @var ItemRepositoryInterface */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->make(ItemRepositoryInterface::class);
        factory(Basket::class)->create();
    }

    public function testGetByIdExist()
    {
        $data = $this->data();
        factory(ItemApple::class)->create($data);
        $item = $this->repository->getById(1);
        $this->assertInstanceOf(Item::class, $item);
    }

    public function testGetByIdNotExist()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->repository->getById(1);
    }

    public function testSave()
    {
        $data = $this->data();
        $item = factory(ItemApple::class)->make($data);
        $dbItem = $this->repository->save($item);
        $this->assertInstanceOf(Item::class, $dbItem);
        $this->assertDatabaseHas('items', $data);
    }

    public function testDelete()
    {
        $data = $this->data();
        $item = factory(ItemApple::class)->create($data);
        $this->assertDatabaseHas('items', $data);
        $deleted = $this->repository->delete($item);
        $this->assertTrue($deleted);
        $this->assertDatabaseMissing('items', $data);
    }

    public function testDeleteNotExistItem()
    {
        $data = $this->data();
        $item = factory(ItemApple::class)->make($data);
        $deleted = $this->repository->delete($item);
        $this->assertFalse($deleted);
        $this->assertDatabaseMissing('items', $data);
    }

    public function testFindAll()
    {
        factory(ItemApple::class, 7)->create();
        $items = $this->repository->findAll();

        $this->assertInstanceOf(Collection::class, $items);
        $this->assertNotEmpty($items);
        foreach ($items as $item){
            $this->assertInstanceOf(Item::class, $item);
        }
    }

    public function testFindAllForEmptyTable()
    {
        $items = $this->repository->findAll();
        $this->assertInstanceOf(Collection::class, $items);
        $this->assertEmpty($items);
    }

    public function testFindByBasketId()
    {
        $basket = Basket::query()->first();
        $basketSecond = factory(Basket::class)->create();
        factory(ItemApple::class, 4)->create([
            'basket_id' => $basketSecond->id
        ]);
        factory(ItemApple::class, 7)->create();
        $items = $this->repository->findByBasketId($basket->id);

        $this->assertInstanceOf(Collection::class, $items);
        $this->assertNotEmpty($items);
        foreach ($items as $item){
            $this->assertInstanceOf(Item::class, $item);
            $this->assertEquals($basket->id, $item->basket_id );
        }
    }

    private  function data()
    {
        return [
            'type' => 'apple',
            'weight' => 150
        ];
    }
}
