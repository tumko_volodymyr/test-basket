<?php

namespace Tests\Feature;

use App\Model\Contracts\Entities\ItemInterface;
use App\Model\Entities\Basket;
use App\Model\Entities\ItemApple;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemApiTest extends TestCase
{
    use RefreshDatabase;

    const JSON_ERROR_STRUC = [
        'error' =>
            ['message']
    ];

    const JSON_ITEN_STRUC = [
        'id',
        'type',
        'weight',
        'basket_id'
    ];

    const API_URL_ROOT = '/api/v1/items';

    public function testDelete()
    {
        factory(Basket::class)->create();
        $item = factory(ItemApple::class)->create();
        $response = $this->json('DELETE', sprintf('%s/%s', self::API_URL_ROOT, $item->id));

        $response->assertStatus(Response::HTTP_NO_CONTENT);

    }

    public function testDeleteNotExist()
    {
        factory(Basket::class)->create();
        $item = factory(ItemApple::class)->create();
        $response = $this->json('DELETE', sprintf('%s/%s', self::API_URL_ROOT, $item->id+1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    public function testItemsCreationSuccess()
    {
        $basket = factory(Basket::class)->create(['max_capacity' => 10000]);
        $itemsData = [
            [
                'type' => ItemInterface::TYPE_APPLE,
                'weight' => 3000,
                'basket_id' => $basket->id
            ],
            [
                'type' => ItemInterface::TYPE_ORANGE,
                'weight' => 3000,
                'basket_id' => $basket->id
            ],
            [
                'type' => ItemInterface::TYPE_WATERMELON,
                'weight' => 3000,
                'basket_id' => $basket->id
            ]
        ];
        $data = [
            'items' => $itemsData
        ];
        $response = $this->json('POST', self::API_URL_ROOT.'/array', $data);

        $response->assertOk()
            ->assertJsonStructure(['data' => [
                '*' => self::JSON_ITEN_STRUC
            ]])
            ->assertJson([
                'data' => $itemsData
            ])
        ;
    }



    function testItemsCreationWithNoExistType()
    {
        $basket = factory(Basket::class)->create();
        $itemsData = [
            [
                'type' => 'notExistItemType',
                'weight' => 3000,
                'basket_id' => $basket->id
            ]
        ];
        $data = [
            'items' => $itemsData
        ];
        $response = $this->json('POST', self::API_URL_ROOT.'/array', $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing(['data' => $itemsData]);
    }

    function testItemsCreationWithOverFullfill()
    {
        $basket = factory(Basket::class)->create(['max_capacity' => 10000]);
        $itemsData = [
            [
                'type' => ItemInterface::TYPE_APPLE,
                'weight' => 3000,
                'basket_id' => $basket->id
            ],
            [
                'type' => ItemInterface::TYPE_ORANGE,
                'weight' => 3000,
                'basket_id' => $basket->id
            ],
            [
                'type' => ItemInterface::TYPE_WATERMELON,
                'weight' => 5000,
                'basket_id' => $basket->id
            ]
        ];
        $data = [
            'items' => $itemsData
        ];
        $response = $this->json('POST', self::API_URL_ROOT.'/array', $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing(['data' => $itemsData]);
    }

    function testItemsCreationWithValidWeight()
    {
        $basket = factory(Basket::class)->create(['max_capacity' => 10000]);
        $itemsData = [
            [
                'type' => ItemInterface::TYPE_APPLE,
                'weight' => -3000,
                'basket_id' => $basket->id
            ]
        ];
        $data = [
            'items' => $itemsData
        ];
        $response = $this->json('POST', self::API_URL_ROOT.'/array', $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing(['data' => $itemsData]);
    }

    function testItemsCreationWithValidBassketId()
    {
        $basket = factory(Basket::class)->create(['max_capacity' => 10000]);
        $itemsData = [
            [
                'type' => ItemInterface::TYPE_APPLE,
                'weight' => 3000,
                'basket_id' => $basket->id+1
            ]
        ];
        $data = [
            'items' => $itemsData
        ];
        $response = $this->json('POST', self::API_URL_ROOT.'/array', $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing(['data' => $itemsData]);
    }
}
