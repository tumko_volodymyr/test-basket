<?php

namespace Tests\Feature;

use App\Model\Entities\Basket;
use App\Model\Entities\ItemApple;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BasketApiTest extends TestCase
{
    use RefreshDatabase;

    const JSON_BASKET_STRUC = [
        'id',
        'name',
        'max_capacity'
    ];

    const JSON_ERROR_STRUC = [
        'error' =>
            ['message']
    ];

    const JSON_STRUCTURE = ['data' => self::JSON_BASKET_STRUC];
    const JSON_LIST_STRUCTURE = ['data' => [
        '*' => self::JSON_BASKET_STRUC
    ]];

    const API_URL_ROOT = '/api/v1/baskets';

    public function testAllExist()
    {
        factory(Basket::class, 3)->create();
        $response = $this->json('GET', self::API_URL_ROOT);

        $response->assertOk()
            ->assertJsonStructure(self::JSON_LIST_STRUCTURE);;

    }

    public function testStore()
    {
        $data = [
            'name' => 'Apple',
            'max_capacity' => 500
        ];
        $response = $this->json('POST', self::API_URL_ROOT, $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(self::JSON_STRUCTURE)
            ->assertJsonFragment($data)
        ;

    }

    public function testStoreInvalidName()
    {
        $data = [
            'max_capacity' => 500
        ];
        $response = $this->json('POST', self::API_URL_ROOT, $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;

    }

    public function testStoreInvalidMaxCapacity()
    {
        $data = [
            'name' => 'No max capacity'
        ];
        $response = $this->json('POST', self::API_URL_ROOT, $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;

    }

    public function testShow()
    {
        $data = [
            'name' => 'Apple',
            'max_capacity' => 500
        ];
        $basket = factory(Basket::class)->create($data);
        $response = $this->json('GET', sprintf('%s/%s', self::API_URL_ROOT, $basket->id));

        $response->assertOk()
            ->assertJsonStructure(self::JSON_STRUCTURE)
            ->assertJsonFragment($data)
        ;

    }

    public function testShowNotExist()
    {
        $data = [
            'name' => 'Apple',
            'max_capacity' => 500
        ];
        $basket = factory(Basket::class)->create($data);
        $response = $this->json('GET', sprintf('%s/%s', self::API_URL_ROOT, $basket->id+1));

        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;

    }

    public function testDelete()
    {
        $basket = factory(Basket::class)->create();
        $response = $this->json('DELETE', sprintf('%s/%s', self::API_URL_ROOT, $basket->id));

        $response->assertStatus(Response::HTTP_NO_CONTENT);

    }

    public function testDeleteNotExist()
    {
        $basket = factory(Basket::class)->create();
        $response = $this->json('DELETE', sprintf('%s/%s', self::API_URL_ROOT, $basket->id+1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    public function testUpdate()
    {
        $data = [
            'name' => 'Apple New',
        ];
        $basket = factory(Basket::class)->create();
        $response = $this->json(
            'PUT',
            sprintf('%s/%s', self::API_URL_ROOT, $basket->id),
            $data
        );

        $response->assertOk()
            ->assertJsonStructure(self::JSON_STRUCTURE)
            ->assertJsonFragment($data)
        ;
    }

    public function testUpdateNotValidName()
    {
        $data = [
            'name' => '',
        ];
        $basket = factory(Basket::class)->create();
        $response = $this->json(
            'PUT',
            sprintf('%s/%s', self::API_URL_ROOT, $basket->id),
            $data
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;
    }

    public function testUpdateNotExist()
    {
        $data = [
            'name' => 'Updated',
        ];
        $basket = factory(Basket::class)->create();
        $response = $this->json(
            'PUT',
            sprintf('%s/%s', self::API_URL_ROOT, $basket->id+1),
            $data
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;
    }

    public function testShowBasketItemsEmpty()
    {
        $data = [];
        $basket = factory(Basket::class)->create();
        $response = $this->json('GET', sprintf('%s/%s/items', self::API_URL_ROOT, $basket->id));

        $response->assertOk()
            ->assertJson($data)
        ;
    }

    public function testShowBasketItemsNotExist()
    {
        $data = [];
        $basket = factory(Basket::class)->create();
        $response = $this->json('GET', sprintf('%s/%s/items', self::API_URL_ROOT, $basket->id+1));

        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJsonStructure(self::JSON_ERROR_STRUC)
            ->assertJsonMissing($data)
        ;
    }


    public function testShowBasketItems()
    {
        $oneStructure = [
            'id',
            'basket_id',
            'type',
            'weight',
        ];
        $basket = factory(Basket::class)->create();
        factory(ItemApple::class, 7)->create();
        $response = $this->json('GET', sprintf('%s/%s/items', self::API_URL_ROOT, $basket->id));

        $response->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => $oneStructure
                ]
            ])
            ->assertJsonFragment(['basket_id' => $basket->id])
        ;

    }
}
