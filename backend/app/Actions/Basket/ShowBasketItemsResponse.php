<?php


namespace App\Actions\Basket;


use App\Common\HasCollections;

final class ShowBasketItemsResponse
{
    use HasCollections;
}

