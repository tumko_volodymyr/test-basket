<?php


namespace App\Actions\Basket;


final class UpdateAction
{
    use HasBasketRepository;

    public function execute(UpdateRequest $request): UpdateResponse
    {
        $basket = $this->repository->getById($request->getId());
        $basket->name = $request->getName();

        $this->repository->save($basket);

        return new UpdateResponse($basket);
    }
}

