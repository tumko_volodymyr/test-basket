<?php


namespace App\Actions\Basket;


use App\Http\Requests\Basket\CreateHttpRequest;

final class CreateRequest
{

    private $name;
    private $maxCapacity;

    public function __construct(string $name, int $maxCapacity)
    {
        $this->name = $name;
        $this->maxCapacity = $maxCapacity;
    }

    public static function fromRequest(CreateHttpRequest $request): self
    {
        return new self($request->getName(), $request->getMaxCapacity());
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMaxCapacity(): int
    {
        return $this->maxCapacity;
    }
}
