<?php


namespace App\Actions\Basket;


use App\Model\Entities\Basket;

trait HasBasket
{
    private $basket;

    public function __construct(Basket $basket)
    {
        $this->basket = $basket;
    }

    public function getBasket(): Basket
    {
        return $this->basket;
    }
}
