<?php


namespace App\Actions\Basket;


use App\Common\HasId;

final class UpdateRequest
{
    use HasId;

    private $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
