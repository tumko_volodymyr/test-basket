<?php


namespace App\Actions\Basket;


final class AllAction
{
    use HasBasketRepository;

    public function execute(): AllResponse
    {
        return new AllResponse($this->repository->findAll());
    }
}

