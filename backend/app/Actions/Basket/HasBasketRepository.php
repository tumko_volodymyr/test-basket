<?php


namespace App\Actions\Basket;


use App\Model\Contracts\Repositories\BasketRepositoryInterface;

trait HasBasketRepository
{
    private $repository;

    public function __construct(BasketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
}

