<?php


namespace App\Actions\Basket;


final class DeleteAction
{
    use HasBasketRepository;

    public function execute(DeleteRequest $request): void
    {
        $basket = $this->repository->getById($request->getId());
        $this->repository->delete($basket);
    }
}

