<?php


namespace App\Actions\Basket;


use App\Common\HasId;

final class ShowBasketItemsRequest
{
    use HasId;
}
