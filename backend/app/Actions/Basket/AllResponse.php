<?php


namespace App\Actions\Basket;


use App\Common\HasCollections;
use Illuminate\Support\Collection;

final class AllResponse
{
    use HasCollections;

    public function getBaskets(): Collection
    {
        return $this->getCollection();
    }
}

