<?php


namespace App\Actions\Basket;


use App\Common\HasId;

final class ShowRequest
{
    use HasId;
}
