<?php


namespace App\Actions\Basket;


final class ShowBasketItemsAction
{
    use HasBasketRepository;

    public function execute(ShowBasketItemsRequest $request): ShowBasketItemsResponse
    {
        return new ShowBasketItemsResponse($this->repository->getById($request->getId())->items);
    }
}

