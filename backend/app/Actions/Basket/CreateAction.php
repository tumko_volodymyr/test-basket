<?php


namespace App\Actions\Basket;


use App\Model\Entities\Basket;

final class CreateAction
{
    use HasBasketRepository;

    public function execute(CreateRequest $request): CreateResponse
    {
        $basket = new Basket([
            'name' => $request->getName(),
            'max_capacity' => $request->getMaxCapacity()
        ]);

        $this->repository->save($basket);

        return new CreateResponse($basket);
    }
}

