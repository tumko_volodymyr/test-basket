<?php


namespace App\Actions\Basket;


final class ShowAction
{
    use HasBasketRepository;

    public function execute(ShowRequest $request): ShowResponse
    {
        return new ShowResponse($this->repository->getById($request->getId()));
    }
}

