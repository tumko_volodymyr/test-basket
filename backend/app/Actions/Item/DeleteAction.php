<?php


namespace App\Actions\Item;


final class DeleteAction
{
    use HasItemRepository;

    public function execute(DeleteRequest $request): void
    {
        $item = $this->repository->getById($request->getId());
        $this->repository->delete($item);
    }
}

