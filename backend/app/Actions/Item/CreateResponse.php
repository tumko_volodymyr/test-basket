<?php


namespace App\Actions\Item;


use App\Common\HasCollections;

final class CreateResponse
{
    use HasCollections;
}

