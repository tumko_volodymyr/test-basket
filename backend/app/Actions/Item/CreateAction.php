<?php


namespace App\Actions\Item;


use App\Exceptions\AppException;
use App\Exceptions\AppInvalidArgumentException;
use App\Model\Contracts\Factories\ItemFactoryInterface;
use App\Model\Contracts\Repositories\ItemRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class CreateAction
{

    private $repository;
    private $factory;

    public function __construct(ItemRepositoryInterface $repository, ItemFactoryInterface $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function execute(CreateRequest $request): CreateResponse
    {
        $collection = new Collection();
        DB::beginTransaction();
        foreach ($request->getItems() as $itemAttributes)
        {
            try {
                $type = $itemAttributes['type']??null;
                $weight = $itemAttributes['weight']??null;
                $basketId =$itemAttributes['basket_id']??null;
                $item = $this->factory->create($type, $weight, $basketId);
                $this->repository->save($item);
                if ($item->getBasket()->getMaxCapacity() - $item->getBasket()->getCapacity() < 0){
                    throw new AppInvalidArgumentException('Basket cannot fill all new items');
                }
                $collection->add($item);
            } catch(AppException $exception) {
                DB::rollBack();
                throw $exception;
            }
        }
        DB::commit();
        return new CreateResponse($collection);
    }

}

