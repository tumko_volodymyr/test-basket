<?php


namespace App\Actions\Item;


use App\Http\Requests\Item\CreateHttpRequest;

final class CreateRequest
{
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public static function fromRequest(CreateHttpRequest $request): self
    {
        return new self($request->getItems());
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
