<?php


namespace App\Actions\Item;


use App\Common\HasId;

final class DeleteRequest
{
    use HasId;
}
