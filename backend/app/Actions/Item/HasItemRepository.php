<?php


namespace App\Actions\Item;


use App\Model\Contracts\Repositories\ItemRepositoryInterface;

trait HasItemRepository
{
    private $repository;

    public function __construct(ItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
}

