<?php

namespace App\Providers;

use App\Model\Contracts\Factories\ItemFactoryInterface;
use App\Model\Contracts\Repositories\BasketRepositoryInterface;
use App\Model\Contracts\Repositories\ItemRepositoryInterface;
use App\Model\Factories\ItemFactory;
use App\Model\Repositories\EloquentBasketRepository;
use App\Model\Repositories\EloquentItemRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BasketRepositoryInterface::class, EloquentBasketRepository::class);
        $this->app->bind(ItemRepositoryInterface::class, EloquentItemRepository::class);
        $this->app->bind(ItemFactoryInterface::class, ItemFactory::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
