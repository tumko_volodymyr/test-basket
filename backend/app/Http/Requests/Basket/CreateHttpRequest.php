<?php


namespace App\Http\Requests\Basket;


use App\Http\Request\ApiFormRequest;

final  class CreateHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'max_capacity' => 'required|numeric|min:1',
        ];
    }

    public function getName(): string
    {
        return $this->get('name');
    }

    public function getMaxCapacity(): int
    {
        return (int)$this->get('max_capacity');
    }
}
