<?php


namespace App\Http\Requests\Basket;


use App\Http\Request\ApiFormRequest;

final class UpdateHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string',
        ];
    }

    public function getName(): string
    {
        return $this->get('name');
    }
}
