<?php


namespace App\Http\Requests\Item;


use App\Http\Request\ApiFormRequest;

final  class CreateHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'items' => 'required|array|min:1',
            'items.*.type' => 'required|string|min:1',
            'items.*.weight' => 'required|integer|min:1',
            'items.*.basket_id' => 'required|integer|min:1',
        ];
    }

    public function filters()
    {
        return [
            'items.*.type' => 'trim|lowercase',
        ];
    }

    public function getItems(): array
    {
        return $this->get('items');
    }
}
