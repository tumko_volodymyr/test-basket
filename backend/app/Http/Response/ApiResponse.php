<?php


namespace App\Http\Response;

use App\Contracts\Api\ApiExceptionInterface;
use App\Contracts\Api\ApiResponseInterface;
use Illuminate\Http\JsonResponse;

final class ApiResponse extends JsonResponse
{
    public static function success(ApiResponseInterface $response, int $code = 200): self
    {
        return new static(
            [
                'data' => $response
            ],
            $code
        );
    }

    public static function emptySuccess(int $code = 200): self
    {
        return new static([], $code);
    }

    public static function error(ApiExceptionInterface $exception): self
    {
        return new static([
            'error' => $exception->toArray()
        ], $exception->getStatus());
    }
}
