<?php


namespace App\Http\Resources;

use App\Contracts\Api\ApiResponseInterface;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class BasketResourceCollection extends ResourceCollection implements ApiResponseInterface
{
    public function toArray($request): array
    {
        return $this->collection->toArray();
    }
}
