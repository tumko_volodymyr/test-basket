<?php

namespace App\Http\Resources;

use App\Contracts\Api\ApiResponseInterface;
use Illuminate\Http\Resources\Json\JsonResource;

class BasketResource extends JsonResource implements ApiResponseInterface
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'max_capacity' => $this->max_capacity,
        ];
    }
}
