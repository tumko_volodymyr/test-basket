<?php

namespace App\Http\Resources;

use App\Contracts\Api\ApiResponseInterface;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource implements ApiResponseInterface
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'basket_id' => $this->basket_id,
            'type' => $this->type,
            'weight' => $this->weight,
        ];
    }
}
