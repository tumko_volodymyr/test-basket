<?php

namespace App\Http\Controllers\Api;

use App\Actions\Item\CreateAction;
use App\Actions\Item\CreateRequest;
use App\Actions\Item\DeleteAction;
use App\Actions\Item\DeleteRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Item\CreateHttpRequest;
use App\Http\Resources\ItemResourceCollection;
use App\Http\Response\ApiResponse;
use Illuminate\Http\Response;

class ItemController extends Controller
{
    public function delete(DeleteAction $action, int $id): ApiResponse
    {
        $action->execute(new DeleteRequest($id));
        return ApiResponse::emptySuccess(Response::HTTP_NO_CONTENT);
    }

    public function createItems(CreateHttpRequest $request, CreateAction $action): ApiResponse
    {
        $response = $action->execute(CreateRequest::fromRequest($request));
        return ApiResponse::success(new ItemResourceCollection($response->getCollection()));
    }
}
