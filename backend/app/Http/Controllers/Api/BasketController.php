<?php

namespace App\Http\Controllers\Api;

use App\Actions\Basket\AllAction;
use App\Actions\Basket\CreateAction;
use App\Actions\Basket\CreateRequest;
use App\Actions\Basket\DeleteAction;
use App\Actions\Basket\DeleteRequest;
use App\Actions\Basket\ShowAction;
use App\Actions\Basket\ShowBasketItemsAction;
use App\Actions\Basket\ShowBasketItemsRequest;
use App\Actions\Basket\ShowRequest;
use App\Actions\Basket\UpdateAction;
use App\Actions\Basket\UpdateRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Basket\CreateHttpRequest;
use App\Http\Requests\Basket\UpdateHttpRequest;
use App\Http\Resources\BasketResource;
use App\Http\Resources\BasketResourceCollection;
use App\Http\Resources\ItemResourceCollection;
use App\Http\Response\ApiResponse;
use Illuminate\Http\Response;

class BasketController extends Controller
{
    public function create(CreateHttpRequest $request, CreateAction $action): ApiResponse
    {
        $response = $action->execute(CreateRequest::fromRequest($request));
        return ApiResponse::success(new BasketResource($response->getBasket()),Response::HTTP_CREATED);
    }

    public function all(AllAction $action): ApiResponse
    {
        $response = $action->execute();
        return ApiResponse::success(new BasketResourceCollection($response->getBaskets()));
    }

    public function show(ShowAction $action, int $id): ApiResponse
    {
        $response = $action->execute(new ShowRequest($id));
        return ApiResponse::success(new BasketResource($response->getBasket()));
    }

    public function delete(DeleteAction $action, int $id): ApiResponse
    {
        $action->execute(new DeleteRequest($id));
        return ApiResponse::emptySuccess(Response::HTTP_NO_CONTENT);
    }

    public function update(UpdateHttpRequest $request, UpdateAction $action, int $id): ApiResponse
    {
        $response = $action->execute(new UpdateRequest($id, $request->getName()));
        return ApiResponse::success(new BasketResource($response->getBasket()));
    }

    public function basketItems(ShowBasketItemsAction $action, int $id): ApiResponse
    {
        $response = $action->execute(new ShowBasketItemsRequest($id));
        return ApiResponse::success(new ItemResourceCollection($response->getCollection()));
    }
}
