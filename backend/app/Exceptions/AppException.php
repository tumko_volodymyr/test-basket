<?php

namespace App\Exceptions;

use App\Contracts\Api\ApiExceptionInterface;
use Illuminate\Http\Response;

class AppException extends \Exception implements ApiExceptionInterface
{
    public function getStatus(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message
        ];
    }
}
