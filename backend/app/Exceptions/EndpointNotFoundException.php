<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Http\Response;
use Throwable;

class EndpointNotFoundException extends AppException
{
    public function __construct($message = 'Endpoint not found.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getStatus(): int
    {
        return Response::HTTP_NOT_FOUND;
    }
}
