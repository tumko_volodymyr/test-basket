<?php

namespace App\Exceptions;

use Throwable;

final class ApiValidationException extends AppException
{
    public function __construct($message = 'Failed validation.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
