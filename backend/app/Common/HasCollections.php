<?php


namespace App\Common;


use Illuminate\Support\Collection;

trait HasCollections
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function getCollection(): Collection
    {
        return $this->collection;
    }
}
