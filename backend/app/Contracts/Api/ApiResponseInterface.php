<?php

namespace App\Contracts\Api;

interface ApiResponseInterface
{
    public function toArray($request): array;
}
