<?php

namespace App\Contracts\Api;

interface ApiExceptionInterface
{
    public function getStatus(): int;
    public function toArray(): array;
}
