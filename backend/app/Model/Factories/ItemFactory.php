<?php


namespace App\Model\Factories;


use App\Exceptions\AppInvalidArgumentException;
use App\Model\Contracts\Entities\ItemInterface;
use App\Model\Contracts\Factories\ItemFactoryInterface;
use App\Model\Contracts\Repositories\BasketRepositoryInterface;
use App\Model\Entities\ItemApple;
use App\Model\Entities\ItemOrange;
use App\Model\Entities\ItemWatermelon;
use App\Model\Exceptions\BasketNotFoundException;

class ItemFactory implements ItemFactoryInterface
{

    private $basketRepository;

    public function __construct(BasketRepositoryInterface $basketRepository)
    {
        $this->basketRepository = $basketRepository;
    }

    /**
     * @throws AppInvalidArgumentException
     */
    public function create(string $type, int $weight, int $basketId): ItemInterface
    {
        if ($weight <= 0 ){
            throw new AppInvalidArgumentException(sprintf('Invalid item weight - \'%s\'', $weight));
        }

        try{
            $basket = $this->basketRepository->getById($basketId);
        } catch (BasketNotFoundException $exception) {
            throw new AppInvalidArgumentException(sprintf('Invalid basket id - \'%s\'', $basketId));
        }

        $attributes = [
            'type' => $type,
            'weight' => $weight,
            'basket_id' => $basket->getId()
        ];
        switch ($type){
            case ItemInterface::TYPE_APPLE : return new ItemApple($attributes);
            case ItemInterface::TYPE_ORANGE : return new ItemOrange($attributes);
            case ItemInterface::TYPE_WATERMELON : return new ItemWatermelon($attributes);
        }

        throw new AppInvalidArgumentException(sprintf('Invalid item type - \'%s\'', $type));
    }
}
