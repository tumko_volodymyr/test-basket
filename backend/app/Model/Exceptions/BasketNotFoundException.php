<?php


namespace App\Model\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

final class BasketNotFoundException extends NotFoundHttpException
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Basket not found.', $previous);
    }
}
