<?php


namespace App\Model\Repositories;


use App\Model\Contracts\Repositories\BasketRepositoryInterface;
use App\Model\Entities\Basket;
use App\Model\Exceptions\BasketNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class EloquentBasketRepository implements BasketRepositoryInterface
{

    public function getById(int $id): Basket
    {
        try{
            return Basket::query()->findOrFail($id);
        }catch (ModelNotFoundException $exception){
            throw new BasketNotFoundException($exception);
        }
    }

    public function save(Basket $basket): Basket
    {
        $basket->save();
        return $basket;
    }

    public function delete(Basket $basket): bool
    {
        return (bool)$basket->delete();
    }

    public function findAll(): Collection
    {
        return Basket::all();
    }
}
