<?php


namespace App\Model\Repositories;


use App\Model\Contracts\Repositories\ItemRepositoryInterface;
use App\Model\Entities\Item;
use App\Model\Exceptions\ItemNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class EloquentItemRepository implements ItemRepositoryInterface
{

    public function getById(int $id): Item
    {
        try{
            return Item::query()->findOrFail($id);
        }catch (ModelNotFoundException $exception){
            throw new ItemNotFoundException($exception);
        }
    }

    public function save(Item $item): Item
    {
        $item->save();
        return $item;
    }

    public function delete(Item $item): bool
    {
        return (bool)$item->delete();
    }

    public function findAll(): Collection
    {
        return Item::all();
    }

    public function findByBasketId(int $id): Collection
    {
        return Item::query()->where('basket_id', '=', $id)->get();
    }
}
