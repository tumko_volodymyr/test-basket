<?php

namespace App\Model\Entities;

use App\Model\Contracts\Entities\BasketInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property int $max_capacity
 *
 */
class Basket extends Model implements BasketInterface
{
    protected $fillable = [
        'name', 'max_capacity'
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMaxCapacity(): int
    {
        return $this->max_capacity;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function getCapacity(): int
    {
        return $this->getItems()->reduce(function ($carry, $item) {
            return $carry + $item->getWeight();
        }, 0);
    }
}
