<?php

namespace App\Model\Entities;

use App\Model\Contracts\Entities\BasketInterface;
use App\Model\Contracts\Entities\ItemInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $type
 * @property int $weight
 * @property int $basket_id
 *
 */
class Item extends Model implements ItemInterface
{
    protected $fillable = [
        'weight', 'basket_id'
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getBasket(): BasketInterface
    {
        return $this->basket;
    }

    public function basket()
    {
        return $this->belongsTo(Basket::class);
    }
}
