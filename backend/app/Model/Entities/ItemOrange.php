<?php


namespace App\Model\Entities;


final class ItemOrange extends Item
{
    protected $table = 'items';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->type = self::TYPE_ORANGE;
    }
}
