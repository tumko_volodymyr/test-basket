<?php


namespace App\Model\Entities;


final class ItemWatermelon extends Item
{
    protected $table = 'items';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->type = self::TYPE_WATERMELON;
    }
}
