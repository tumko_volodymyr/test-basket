<?php


namespace App\Model\Contracts\Entities;


use Illuminate\Support\Collection;

interface BasketInterface
{
    public function getId(): int;
    public function getName(): string;
    public function getCapacity(): int;
    public function getMaxCapacity(): int;
    public function getItems(): Collection;
}
