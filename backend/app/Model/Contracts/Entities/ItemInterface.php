<?php


namespace App\Model\Contracts\Entities;


interface ItemInterface
{
    const TYPE_APPLE = 'apple';
    const TYPE_ORANGE = 'orange';
    const TYPE_WATERMELON = 'watermelon';

    const TYPES = [
        self::TYPE_APPLE,
        self::TYPE_ORANGE,
        self::TYPE_WATERMELON
    ];

    public function getId(): int;
    public function getType(): string;
    public function getWeight(): int;
    public function getBasket(): BasketInterface;
}
