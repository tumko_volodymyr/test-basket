<?php


namespace App\Model\Contracts\Factories;


use App\Model\Contracts\Entities\ItemInterface;

interface ItemFactoryInterface
{
    public function create(string $type, int $weight, int $basketId): ItemInterface;
}
