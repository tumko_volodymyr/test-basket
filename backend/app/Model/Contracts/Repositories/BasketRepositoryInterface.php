<?php


namespace App\Model\Contracts\Repositories;


use App\Model\Entities\Basket;
use Illuminate\Support\Collection;

interface BasketRepositoryInterface
{
    public function getById(int $id): Basket;
    public function save(Basket $basket): Basket;
    public function delete(Basket $basket): bool;
    public function findAll(): Collection;
}
