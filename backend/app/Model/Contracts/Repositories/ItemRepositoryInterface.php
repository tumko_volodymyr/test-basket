<?php


namespace App\Model\Contracts\Repositories;


use App\Model\Entities\Item;
use Illuminate\Support\Collection;

interface ItemRepositoryInterface
{
    public function getById(int $id): Item;
    public function save(Item $item): Item;
    public function delete(Item $item): bool;
    public function findAll(): Collection;
    public function findByBasketId(int $id): Collection;
}
